#!/usr/bin/env python3
"""Kacper Wenta """


class LivingCreature:
    def __init__(self):
        print("Created Living creature")
        print(self)

    def __del__(self):
        print("Destructed an object", self)

    def breathing(self):
        print(self, "Im alive!")

class Animal(LivingCreature):
    def __init__(self):
        super(Animal, self).__init__()
        print("Created animal")
        print(self)

    def breathing(self):
        print("I'm taking a breath")

class Plant(LivingCreature):
    def __init__(self):
        super(Plant, self).__init__()
        print("Created plant")
        print(self)


lc = LivingCreature()
animal = Animal()
plant = Plant()

animal.breathing()

