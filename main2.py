#!/usr/bin/env python3
"""Kacper Wenta"""


class Human:
    """Class containing info about human"""
    def __init__(self, name, surname, age):
        """Ctor"""
        self.name = name
        self.surname = surname
        self.age = age

    def __repr__(self):
        """Print representation"""
        return f"{self.name} {self.surname}"


kacper = Human("Kacper", "Wenta", 22)
print(kacper)
